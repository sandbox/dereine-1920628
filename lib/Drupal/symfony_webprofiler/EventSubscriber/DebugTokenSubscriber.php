<?php

/**
 * @file
 * Contains \Drupal\symfony_webprofiler\EventSubscriber\DebugTokenSubscriber.
 */

namespace Drupal\symfony_webprofiler\EventSubscriber;

use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Event\FilterResponseEvent;
use Symfony\Component\HttpKernel\KernelEvents;
use Twig_Loader_Filesystem;

class DebugTokenSubscriber implements EventSubscriberInterface {

  /**
   * Constructs a DebugTokenSubscriber object.
   */
  public function __construct(Twig_Loader_Filesystem $twig_loader) {
    $twig_loader->addPath(DRUPAL_ROOT . '/modules/symfony_webprofiler/vendor/symfony/web-profiler-bundle/Symfony/Bundle/WebProfilerBundle/Resources/views', 'WebProfiler');
  }

  /**
   * Adds the X-Debug-Token header to the response.
   */
  public function onKernelResponse(FilterResponseEvent $event) {
    $response = $event->getResponse();
    $response->headers->set('X-Debug-Token', 1);
  }

  /**
   * Implements EventSubscriberInterface::getSubscribedEvents().
   */
  public static function getSubscribedEvents() {
    // Come before WebDebugToolbarListener
    $events[KernelEvents::RESPONSE] = array('onKernelResponse', -120);
    return $events;
  }

}
