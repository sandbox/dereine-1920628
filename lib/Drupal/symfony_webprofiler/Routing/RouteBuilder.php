<?php

namespace Drupal\symfony_webprofiler\Routing;

use Drupal\Core\Routing\RouteBuilder as RouteBuilderDrupal;

class RouteBuilder {
  protected $builders;

  public function __construct(RouteBuilderDrupal $router_builder_drupal) {
    $this->builders[] = $router_builder_drupal;
  }
}
