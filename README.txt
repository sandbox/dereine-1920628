## Installation
  * download the dependencies and remove everything beside symfony/twig-bridge,web-profiler-bundle
  * link these directories into the right locations in core vendor dir.

## Big TODOs

  * How to not install dependencies which are already part of drupal core,
    like the event dispatcher, routing/http components of symfony etc.

  * Provide proper autoloading

